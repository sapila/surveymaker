<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TODO: these could just be one
Route::get('/{path?}/{child?}', function () {
    return view('index');
});

Route::get('/{path?}', function () {
    return view('index');
});

