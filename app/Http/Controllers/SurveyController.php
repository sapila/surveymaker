<?php

namespace App\Http\Controllers;

use App\Factories\RespondentAnswerFactory;
use App\Question;
use App\Respondent;
use App\Survey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SurveyController extends Controller
{
    public function single(string $surveyId)
    {
        /** @var Survey $survey */
        $survey = Survey::where('id', $surveyId)->get()->first();
        if ($survey === null) {
            return new Response(null, 404);
        }

        return new Response($survey, 200);
    }

    public function answer(Request $request, string $surveyId)
    {
        // Input validation
        // check if question ids are part of the survey
        $answers = $request->get('answers');

        // store answers to tables based on question
        /* @var $survey Survey */
        $survey = Survey::where('id', $surveyId)->get()->first();
        if (!$survey) {
            return response(null, 404);
        }

        foreach ($survey->questions as $question) {
            $index = array_search($question->id, array_column($answers, 'questionId'));

            if ($index === false) {
                continue;
            }

            $respondentAnswer = RespondentAnswerFactory::create(
                $question->frontend_answer_type,
                $question->answer->id,
                $answers[$index]['answer']
            );
            /* @var $question Question */
            $question->answer->respondentAnswers()->save($respondentAnswer);
        }

        $survey->respondents()->create([
            'email' => 'anonymous@mail.com'
        ]);

        return new Response(null, 201);
    }
}
