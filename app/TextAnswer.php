<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TextAnswer extends Model
{
    protected $fillable = ['min_chars', 'max_chars'];

    protected $visible = ['min_chars', 'max_chars'];

    public function question()
    {
        return $this->morphOne(Question::class, 'answer');
    }

    public function respondentAnswers()
    {
        return $this->hasMany(RespondentTextAnswer::class);
    }
}
