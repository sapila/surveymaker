<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = ['name', 'title', 'user_id'];

    protected $visible = ['id', 'name', 'title', 'questions', 'respondents_count'];

    protected $with = ['questions'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function respondents()
    {
        return $this->hasMany(Respondent::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
