<?php


namespace App\Services;


use App\MultipleChoiceAnswer;
use App\Question;
use App\RespondentScaleAnswer;
use App\ScaleAnswer;
use App\TextAnswer;
use Illuminate\Support\Facades\DB;

class AnswerResultsService
{
    public function getResultsForAnswer($answer)
    {
        switch (get_class($answer)) {
            case TextAnswer::class:
                return $this->getTextAnswerResults($answer);
            case MultipleChoiceAnswer::class:
                return $this->getMultipleAnswerResults($answer);
            case ScaleAnswer::class:
                return $this->getScaleAnswerResults($answer);
            default:
                throw new \Exception('Unsupported Answer type');
        }
    }

    private function getTextAnswerResults(TextAnswer $answer): array
    {
        $results = [];
        foreach ($answer->respondentAnswers as $respondentAnswer) {
            $results[] = $respondentAnswer->answer;
        };

        return $results;
    }

    private function getScaleAnswerResults(ScaleAnswer $answer): array
    {
        $results = DB::table('respondent_scale_answers')
            ->select(DB::raw('cast(answer as unsigned) as answer'), DB::raw('count(*) as answerCount'))
            ->where('scale_answer_id', $answer->id)
            ->orderBy('answer')
            ->groupBy('answer')
            ->get()
            ->toArray();

        return $results;
    }

    private function getMultipleAnswerResults(MultipleChoiceAnswer $answer): array
    {
        $results = DB::table('respondent_multiple_choice_answers')
            ->join('multiple_choice_answer_options', 'respondent_multiple_choice_answers.multiple_choice_answer_option_id', '=', 'multiple_choice_answer_options.id')
            ->select('multiple_choice_answer_options.text', DB::raw('count(*) as answerCount'))
            ->where('respondent_multiple_choice_answers.multiple_choice_answer_id', $answer->id)
            ->groupBy('multiple_choice_answer_options.text')
            ->get()
            ->toArray();


        return $results;
    }
}
