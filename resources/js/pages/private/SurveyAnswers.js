import React, {Component} from "react";
import {surveyService} from "../../services/SurveyService";
import TextAnswer from "../../components/forms/respondent/TextAnswer";
import ScaleAnswer from "../../components/forms/respondent/ScaleAnswer";
import MultipleChoiceAnswer from "../../components/forms/respondent/MultipleChoiceAnswer";
import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer
} from 'recharts';

class SurveyAnswers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            survey: null
        };
    }

    componentDidMount() {
        surveyService.getAnswersById(this.props.match.params.surveyId)
            .then(response => {
                console.log(response)
                this.setState({ survey: response });
            });
    }

    renderMultipleChoiceAnswers(results) {
        const data = [];
        for (const [index, item] of results.entries()) {
            data.push({
                'name': item.text,
                'count': item.answerCount
            })
        }

        return (
            <ResponsiveContainer width="100%" height={300}>
                <BarChart
                    data={data}
                    margin={{
                        top: 5, right: 0, left: 0, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="count" fill="#3490dc" />
                </BarChart>
            </ResponsiveContainer>
        );
    }

    renderScaleAnswers(results) {
        const data = [];
        for (const [index, item] of results.entries()) {
            data.push({
                'name': item.answer,
                'count': item.answerCount
            })
        }

        return (
            <ResponsiveContainer width="100%" height={300}>
                <BarChart
                    data={data}
                    margin={{
                        top: 5, right: 0, left: 0, bottom: 5,
                    }}
                >
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Bar dataKey="count" fill="#3490dc" />
                </BarChart>
            </ResponsiveContainer>
        );
    }

    renderTextAnswerItem(item, index) {
        return <li className="list-group-item" key={'item' + index}>{item}</li>;
    }

    renderTextAnswers(results) {
        let items = [];

        for (const [index, item] of results.entries()) {
            items.push(this.renderTextAnswerItem(item, index))
        }

        return <div className="overflow-auto" style={ { height: 300 } } >
                    {items}
                </div>;
    }

    renderAnswer(question) {
        let results = null;
        switch (question.frontend_answer_type) {
            case "TextAnswer":
                results = this.renderTextAnswers(question.results);
                break;
            case "ScaleAnswer":
                results = this.renderScaleAnswers(question.results);
                break;
            case "MultipleChoiceAnswer":
                results = this.renderMultipleChoiceAnswers(question.results);
                break;
        }

        return <div>
                <div className="container-fluid col-sm-8 offset-2">
                    {results}
                </div>
            </div>;
    }

    renderQuestionResult(question, index) {
        let answer = this.renderAnswer(question);

        return  (
                    <div>
                        <div className="container-fluid text-center">
                         <h3 key={'question' + index}>{question.text}</h3>
                        </div>
                    {answer}
                    <br /><br />
                    </div>
                )
            ;
    }

    render() {
        if (this.state.survey === null) {
            return <div></div>
        }

        let results = [];
        for (const [index, question] of this.state.survey.questions.entries()) {
            results.push(this.renderQuestionResult(question, index));
        }

        return (
            <div className="container-fluid">
                <div className="container-fluid text-center">
                    <h2 className="font-weight-bold">{this.state.survey.title}</h2>
                </div>
                <br />
                {results}
            </div>
        )
    }
}

export default SurveyAnswers;
