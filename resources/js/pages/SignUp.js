import React, {Component} from "react";
import {Button, FormControl, FormGroup, FormLabel} from "react-bootstrap";
import {Redirect} from "react-router-dom";

class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            password: "",
            password_confirmation: "",
            isRegistrationSuccessful: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        let data = this.state;

        fetch('http://'+ window.$serverIp +'/api/auth/signup', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                'X-Requested-With': 'XMLHttpRequest'},
            body: JSON.stringify(data)
        })
            .then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(() => this.setState(() => ({ isRegistrationSuccessful: true})));
    }

    validateForm() {
        return this.state.name.length > 0 &&
               this.state.email.length > 0 &&
               this.state.password.length > 0 &&
               this.state.password_confirmation.length > 0;
    }

    render() {
        if (this.state.isRegistrationSuccessful) {
            return <Redirect to="/login" />;
        }

        return (
            <div className="container-fluid">
                <div className="row row-no-padding" >
                    <div className="col-sm-3"></div>
                    <div className="Register col-sm-6">
                        <form onSubmit={this.handleSubmit}>
                            <FormGroup controlId="name">
                                <FormLabel>Name</FormLabel>
                                <FormControl autoFocus type="name"
                                             onChange={e => this.setState({ name: e.target.value})}
                                />
                            </FormGroup>
                            <FormGroup controlId="email">
                                <FormLabel>Email</FormLabel>
                                <FormControl autoFocus type="email"
                                             onChange={e => this.setState({ email: e.target.value})}
                                />
                            </FormGroup>
                            <FormGroup controlId="password">
                                <FormLabel>Password</FormLabel>
                                <FormControl type="password"
                                             onChange={e => this.setState({ password: e.target.value})}
                                />
                            </FormGroup>
                            <FormGroup controlId="password_confirmation">
                                <FormLabel>Password Confirmation</FormLabel>
                                <FormControl type="password"
                                             onChange={e => this.setState({ password_confirmation: e.target.value})}
                                />
                            </FormGroup>
                            <Button block disabled={!this.validateForm()} type="submit">
                                Sign Up
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUp;
