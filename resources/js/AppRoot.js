import React, {Component} from "react";
import Header from "./pages/partials/Header";
import Home from "./pages/Home";
import SignUp from "./pages/SignUp";
import LogIn from "./pages/LogIn";
import Dashboard from "./pages/private/Dashboard";
import Surveys from "./pages/Surveys"
import CreateSurvey from "./pages/private/CreateSurvey";
import SurveyAnswers from "./pages/private/SurveyAnswers";

import { PrivateRoute } from './components/PrivateRoute';
import { AnonymousRoute } from './components/AnonymousRoute';

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

class AppRoot extends Component {
    render() {
       return (
           <div>
           <Router>
               <Header />
               <div>
                <Switch>
                    <AnonymousRoute exact path="/" component={Home} />
                    <AnonymousRoute exact path="/sign-up" component={SignUp} />
                    <AnonymousRoute exact path="/login" component={LogIn} />
                    <Route path="/surveys/:surveyId" component={Surveys} />
                    <PrivateRoute exact path="/dashboard" component={Dashboard} />
                    <PrivateRoute exact path="/create-survey" component={CreateSurvey} />
                    <PrivateRoute path="/surveys-answers/:surveyId" component={SurveyAnswers} />
                </Switch>
                </div>
           </Router>
           </div>
       )
    }
}

export default AppRoot;
