import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { authenticationService } from '../services/AuthenticationService';

export const AnonymousRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => {
        if (authenticationService.isAuthenticated()) {
            // not logged in so redirect to login page with the return url
            return <Redirect to={{ pathname: '/dashboard', state: { from: props.location } }} />
        }

        // not authorised so return component
        return <Component {...props} />
    }} />
)
