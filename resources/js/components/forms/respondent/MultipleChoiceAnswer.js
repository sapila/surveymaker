import React, {Component} from "react";

class MultipleChoiceAnswer extends Component {

    constructor(props) {
        super(props);
    }
    handleInputChange(questionId, answer) {
        this.props.onInputChange(questionId, answer);
    }

    render() {
        return this.props.options.map((option, index) => {
            return <div className="form-check" key={index}>
                    <input type="radio" name="question" className="form-check-input"
                           onChange={(e) => this.handleInputChange(this.props.questionId, option.id)}
                    />
                    <label htmlFor="question">{option.text}</label>
                   </div>
        })
    }
}


export default MultipleChoiceAnswer;
