import React, {Component} from "react";

class ScaleAnswer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.handleInputChange(this.props.questionId, this.defaultValue())
    }

    handleInputChange(questionId, answer) {
        this.props.onInputChange(questionId, answer);
    }

    defaultValue() {
        return (this.props.max/2).toString();
    }

    render() {
        return(
            <div className="form-group row">
                <label className="col-sm-1 col-form-label text-center" htmlFor="volume" >{this.props.min}</label>
                <input
                    className="custom-range col-sm-10"
                    type="range" id="start" name="volume"
                    min={this.props.min} max={this.props.max} step="1"
                    data-show-value="true"
                    defaultValue = {this.defaultValue()}
                    onChange={(e) => this.handleInputChange(this.props.questionId, e.target.value)}
                />
                <label className="col-sm-1 col-form-label text-center" htmlFor="volume" >{this.props.max}</label>
            </div>
        )
    }
}


export default ScaleAnswer;
