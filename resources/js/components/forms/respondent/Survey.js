import React, {Component} from "react";
import {surveyService} from "../../../services/SurveyService";
import TextAnswer from "./TextAnswer";
import MultipleChoiceAnswer from "./MultipleChoiceAnswer";
import ScaleAnswer from "./ScaleAnswer";

class Survey extends Component {

    constructor(props) {
        super(props);
        this.state = {
            answeredSuccessfully: false,
            survey:  null,
            answers: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        surveyService.getById(this.props.surveyId)
            .then(response => {
                this.setState({ survey: response });
            });
    }

    handleInputChange(questionId, answer) {
            this.state.answers[questionId] = answer;
    }

    handleSubmit(event) {
        event.preventDefault();
        console.log(this.state.answers);
        surveyService.submitAnswersForSurveyId(this.state.answers, this.props.surveyId)
            .then(response => {
                this.setState({answeredSuccessfully: true});
                console.log(response);
            }).catch(error => {
                // TODO: do something for the error
                console.log('shit hit the fan');
            });
    }

    renderQuestion(question, index) {
        return <div className="container-fluid text-center">
                    <h3 key={'question' + index}>
                        {question.text}
                    </h3>
                </div>;
    }

    renderAnswer(question, index) {
        switch (question.frontend_answer_type) {
            case "TextAnswer":
                return <div className="container-fluid col-sm-8 offset-2">
                            <TextAnswer
                            key={'answer' + index}
                            min_chars={question.answer.min_chars}
                            max_chars={question.answer.max_chars}
                            questionId={question.id}
                            onInputChange={this.handleInputChange.bind(this)}
                            />
                            <br />
                        </div>;
            case "ScaleAnswer":
                return <div className="container-fluid col-sm-8 offset-2">
                            <ScaleAnswer
                            key={'answer' + index}
                            min={question.answer.min}
                            max={question.answer.max}
                            questionId={question.id}
                            onInputChange={this.handleInputChange.bind(this)}
                            />
                            <br />
                        </div>;
            case "MultipleChoiceAnswer":
                return <div className="container-fluid col-sm-6 offset-3">
                            <MultipleChoiceAnswer
                            key={'answer' + index}
                            options={question.answer.options}
                            questionId={question.id}
                            onInputChange={this.handleInputChange.bind(this)}
                            />
                            <br />
                        </div>;
        }
    }

    renderThankYouMessage() {
        return <h3>Thank you for answering the survey!</h3>;
    }

    render() {
        if (!this.state.survey) {
            return '';
        }

        if (this.state.answeredSuccessfully) {
            return this.renderThankYouMessage();
        }

        let questions = [];
        for (const [index, question] of this.state.survey.questions.entries()) {
            questions.push(this.renderQuestion(question, index));
            questions.push(this.renderAnswer(question, index));
        }

        return (
            <div>
                <form onSubmit={this.handleSubmit}>

                    <div className="container-fluid col-sm-8 offset-2 text-center">
                        <h2>{this.state.survey.title}</h2> <br/>
                    </div>
                    {questions}
                    <div className="container-fluid col-sm-6 offset-3">
                        <input type="submit" className="btn btn-primary btn-lg btn-block" value="Submit" />
                    </div>
                </form>
            </div>
        )

    }
}

export default Survey;
