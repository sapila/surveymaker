import React, {Component} from "react";

class TextQuestion extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            config: props.config,
        };
    }

    updateParentState() {
        this.props.updateParentState(this.state.id, this.state.config);
    }

    updateAnswer(configAttribute, value) {
        this.state.config.answer[configAttribute] = value;
        this.updateParentState();
        this.forceUpdate();
    }

    updateQuestionText(value) {
        this.state.config.text = value;
        this.updateParentState();
        this.forceUpdate();
    }

    render() {

        return (
            <div className="container-fluid col-sm-8">
                <div className="card">
                    <div className="card-header">
                        Text Question
                    </div>
                    <div className="card-body">
                        <div className="form-group">
                            <label htmlFor="question">Question text :</label>
                            <input className="form-control" type="text" id='question'
                                   value={this.state.config.text}
                                   onChange={(e) => this.updateQuestionText(e.target.value)} />
                            <label htmlFor="min_chars">Minimum answer characters :</label>
                            <input className="form-control" type="number" id='min_chars'
                                   value={this.state.config.answer.min_chars}
                                   onChange={(e) => this.updateAnswer('min_chars', e.target.value)} />
                            <label htmlFor="max_chars">Maximum answer characters :</label>
                            <input className="form-control" type="number" id='max_chars'
                                   value={this.state.config.answer.max_chars}
                                   onChange={(e) => this.updateAnswer('max_chars', e.target.value)} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TextQuestion;
