<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondentScaleAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_scale_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('scale_answer_id')->nullable();
            $table->string('answer');
            $table->timestamps();

            $table->foreign('scale_answer_id')
                ->references('id')->on('scale_answers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respondent_scale_answers', function (Blueprint $table) {
            $table->dropForeign(['scale_answer_id']);
            $table->dropIfExists();
        });
    }
}
