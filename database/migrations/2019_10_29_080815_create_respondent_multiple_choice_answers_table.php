<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondentMultipleChoiceAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respondent_multiple_choice_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('multiple_choice_answer_option_id')->nullable();
            $table->unsignedBigInteger('multiple_choice_answer_id')->nullable();
            $table->timestamps();

            $table->foreign('multiple_choice_answer_option_id', 'respondent_multiple_choice_option_id_foreign')
                ->references('id')->on('multiple_choice_answer_options')
                ->onDelete('set null');

            $table->foreign('multiple_choice_answer_id', 'respondent_multiple_choice_answer_id_foreign')
                ->references('id')->on('multiple_choice_answers')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('respondent_multiple_choice_answers', function (Blueprint $table) {
            $table->dropForeign('respondent_multiple_choice_option_id_foreign');
            $table->dropForeign('respondent_multiple_choice_answer_id_foreign');
            $table->dropIfExists();
        });
    }
}
